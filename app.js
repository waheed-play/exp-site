const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const mainRoutes = require('./routes');
const cardRoutes = require('./routes/cards');


const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

app.use(express.static('public'));

app.set('view engine', 'pug');

app.use(mainRoutes.mainRoutes);
app.use('/cards', cardRoutes.cardRoutes)

app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error)
});


app.use((err, req, res, next) => {
    res.locals.error = err;
    res.status(err.status);
    res.render('error');
});
app.listen(3000, () => {
    console.log('App is running on port :3000');
})