const express = require('express');
const router = express.Router();


router.get('/', (req, res) => {
    const name = req.cookies.name;
    if(!name) res.redirect('/hello')
    res.render("index",{name});
});

router.get('/hello', (req, res) => {
    res.render("hello");
});

router.post('/hello', (req, res) => {
    const name = req.body.name;
    res.cookie('name', name);
    res.redirect("/", );
});

router.post('/bye', (req, res) => {
    res.clearCookie();
    res.redirect("/hello", );
});

module.exports.mainRoutes =  router;
