const express = require('express');
const router = express.Router();

const { data } = require('../data/flashcardData.json');
const { cards } = data;


router.get('/', (req, res) => {
    const randomId = Math.floor(Math.random() * cards.length);
    res.redirect(`/cards/${randomId}`);
});


router.get('/:id', (req, res) => {

    const { side } = req.query;
    const { id } = req.params;

    if (!side) {
        return res.redirect(`/cards/${id}?side=question`);
    }

    const text = cards[id][side];
    const { hint } = cards[id];

    const templateData = {prompt: text, hint, side, id, name: req.cookies.name};

    if (side === 'question') {
        templateData.sideToShow = 'answer';
        templateData.sideToShowDisplay = 'Answer';
    } else if(side === 'answer'){
        templateData.sideToShow = 'question';
        templateData.sideToShowDisplay = 'Question';
    }else {
        templateData.side = '';
        templateData.prompt = 'Think: binary';
    }

    res.render("cards", templateData);
});

module.exports.cardRoutes =  router;